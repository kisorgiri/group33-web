const express = require('express');
const path = require('path');

// **********for Server side rendering**********88888
// const { App } = require('./src/components/app.component');
// const React = require('react');
// const ReactDOMServer = require('react-dom/server');
// **********for Server side rendering**********88888



const app = express();

// serve static folder
app.use(express.static(path.join(process.cwd(), 'build')))

// accept all incoming request
app.use('/*', function (req, res, next) {
    const mainFile = path.join(process.cwd(), 'build/index.html');

    // **********for Server side rendering**********88888
    // TODO use babel configs to render the below code
    // const appToHtml = ReactDOMServer.renderToString(<App />);
    // **********for Server side rendering**********88888

    res.contentType('text/html');
    res.status(200);
    res.sendFile(mainFile)

})

app.listen(process.env.PORT || 8080, function (err, done) {
    if (!err) {
        console.log('server listening at port ' + process.env.PORT)
    }
})
