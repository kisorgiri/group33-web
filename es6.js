// es6 features

// 1. Object shorthand
// 2.Object destructure
// 3. Rest Operator
// 4. spread Operator
// 5. Arrow Notation FUnction
// 6. template literal syntax
// 7. default argument
// 8. import and export
// 9. class
// 10. block scope

// var phone = 399999

// var phoneOBj = { phone }

// // 1. Object Shorthand
// var kishor = {
//     name: 'kishor',
//     address: 'bkt',
//     phone
// }

// console.log('kishor now >', kishor)

// 2 Object destrcut

function takeSomething() {

    return {
        fruits: 'apple',
        vegitables: 'potato',
        fish: 'fish'
    }
}

// var result = takeSomething();
// var { fruitsdd } = takeSomething();
// console.log('result is >>', result.f)
// console.log('fis is >>', fruitsdd)
var data = {
    a: 'pencil',
    b: 'pen',
    c: 'printer'
}
// var { a: Broadway, c, b } = data;

// console.log('a is >', a);
// console.log('a as boradway >>', Broadway)
// var dataB = data;
// data.a = 'house';
// console.log('data >>', data.a);
// console.log('data b >>', data.a);

// spread operator or rest operator (...)

// spread operator is used to spread the element of the non-primitive datatype

var newObje = {
    xyz: 'hello',
    abc: 'hi',
    c: 'drive'
}
var xyz = {
    ...data,
    ...newObje
}

// xyz.c = 'new printer ';

console.log('xyz c >>', xyz);
// console.log('data is >>', data)


// rest

const { abc: Abc, ...remaininig } = xyz;
console.log('abc only >>', Abc)
console.log('remaininig >>', remaininig)



// Arrow notation function

// function welcome(name, address) {

// }

// const welcome = (name, address) => {
//     // arrow notation function
// }

// const sayHello = addr => {
//     // parenthis is optional for single argument
// }

// const multiply = (num1, num2) => {
//     return num1 * num2
// }

// const multiply = (num1, num2) => num1 * num2;
// one liner function

// main advantages
// traditional function will always starts its own scope as functional scope
// arrow notation function will inherit parent's this (scope)


var laptops = [{
    name: 'xps',
    brand: 'dell'
}, {
    name: 'abc',
    brand: 'xyz'
}, {
    name: 'latitude',
    brand: 'dell'
}, {
    name: 'air',
    brand: 'macbook'
}]

// filter
var dellLaptops = laptops.filter(function (item) {
    if (item.brand === 'dell') {
        return true;
    }
})
console.log('dell laptops >>', dellLaptops);

var arrowDell = laptops.filter(item => item.brand === 'dell');

console.log('arrow dell >>', arrowDell)
var name = 'kishor'
// template literals
var message = `hi ${name} welcome to
brodway`

// var text = 'hi' + name + ' '+abc+' to '+ 'broadway 
// jsdf
// ldjf
// ldjf'


function buySomething(list = { a: 'b' }) {
    console.log('defaut argument >>', list)
    if (list.fruits) {
        console.log('should buy fruits');
    }
    if (list.vegitables) {
        console.log('should buy vegitable')
    }
}

buySomething({
    abc: 'xyz'
});


// import and export

// export
// two ways of export
// 1. named export
// syntax export keyword
// export const Broadway = 'Broadway infosys nepal'
// 2. default export
// syntax export default 
// export default <any value>

// there can multiple named export within a file
// there should be single default export within a file
// there can be both named and default export

// import 
// import totally depends on how it is exported
// syntax import somthing from 'source of file'
// if export is named
// import { boradway, name, address } from './locaiton_to_source_file';

// if export is default
// import any_name from './source_path'

// if both named and default export are present in a file

// import AnyName,{named_1,named_2}  from 'source_path';


// class 
// class is group of fields(properties), methods and constructor
// class is built to create a user define datatype
// example
class Fruits {
    color;
    constructor() {
        // inital value
        this.color = 'yellow';
    }

    getColor() {
        return this.color;
    }
    setColor(newColor) {
        this.color = newColor;
        return this.color;
    }
}

// var apple = new Fruits();
// console.log('apple is >>', apple)

// Inheritance

class Vegitables extends Fruits {
    origin
    constructor() {
        super(); // super keyword is mandatory in heritance 
        // super call is parent's constructor call
        this.origin = 'nepal'
    }

    setOrigin() {
        // this.
    }


}


// var holds functional scope functional block
// let holds block scope {},{},{}
