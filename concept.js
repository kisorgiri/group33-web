// component life cycle hooks
// life cycle can be divided into three parts
// init ==> intial running blocks
// update ===> component's data (props or state ) change huda kheri run hune blocks
// destroy ===> components is  completed

// methods 
// init ==> componentDidMount ,constructor
// update ==> componentDidUpdate
// destory  ===> componenetWillUnmount



// traditional web architecture
// MVC architecture
// concerns ==> Models, Views,COntrollers
// bidirectional data flow between models and controllers

// Flux Architecture
// concerns  ==> views , actions, dispatcher, store
// views ==> react components
// actions ==> rule on each action triggered from view
// dispatcher ==> action should be dispatched to store
// store ==> single source of truth, 
// main state resides on store and update logic also risides in store
// there can be multiple store
// viewss =====> actions====> dispatchers====> store
// unidirectional data flow


// redux
// redux is state management tool
// redux is on top of flux
// concerns ==> views, actions, reducer, store
// views  ==>react components
// actions ==> action generate hune place
// reducers ==> is responsible for updating store
// reducer will have logic to update store
// store ==> centalized state container
// only one(single ) store concept
// views ====> actions====> reducer ===> store
// unidirectional data flow

// tools and librarys to use redux
// redux ==> state management tool
// react-redux ==> bridge to connect with react application and redux store
// middleware(thunk or saga) ==> delayed action dispatch


// redux cycle
// library ==> redux, react-redux, redux-thunk
// redux ==> state management tool (store file,actions folder,reducer folder)
// reac-redux ==> glue to connect react with redux provider ===> to supply store in react app
// connect ==> it is used to connect component with redux
// connect will have mapStateToProps and mapDispatchToProps 
// mapStateToProps will have objects with values taken from root store
// mapDispatchToProps will have actions files
// redux-thunk provides a function to be called to dispatch actions with delay


// store ---> root reducer, initial state, middleware setup

// root component ==> provider to connect store

// actions >>> http call
// reducers ===> logic to update centralized store

// views ===> actions ===> reducers ===> store ===> store contains state 
// changes in centralized state are reflected in component that have used connect with mapStateToProps


// deployement
// server side 
// nodejs based application
// server with nodejs can host our application
// 1. database should kept in cloud
// 2. deploy (heroku)


// client side

//  distribution file
// server le serve
// nodejs kai server
