import React, { Component } from 'react'
import { ProductForm } from '../ProductForm/ProductForm.component'
import { HttpClient } from './../../../utils/httpClient'
import { handleError } from './../../../utils/handleError'
import { notify } from '../../../utils/toastr'

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        HttpClient
            .UPLOAD('POST', '/product', data, files)
            .then(response => {
                notify.showInfo("Product Added Successfully");
                this.props.history.push("view_products");
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                handleError(err)
            });

    }

    render() {
        return (
            <ProductForm
                title="Add Product"
                submitCallback={this.add}
                isSubmitting={this.state.isSubmitting}
            ></ProductForm>
        )
    }
}
