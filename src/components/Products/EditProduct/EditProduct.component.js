import React, { Component } from 'react'
import { handleError } from '../../../utils/handleError';
import { HttpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/toastr';
import { Loader } from '../../Common/Loader/Loader.component';
import { ProductForm } from './../ProductForm/ProductForm.component'

export class EditProduct extends Component {
    constructor() {
        super()

        this.state = {
            isLoading: false,
            isSubmitting: false,
            product: {}
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params['id'];
        HttpClient
            .GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })

    }

    edit = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        HttpClient.UPLOAD('PUT',`/product/${this.productId}`, data, files)
            .then(response => {
                notify.showInfo("Product Updated Successfully")
                this.props.history.push("/view_products")
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm
                isSubmitting={this.state.isSubmitting}
                submitCallback={this.edit}
                title="Update Product"
                productData={this.state.product}
            ></ProductForm>

        return content;
    }
}
