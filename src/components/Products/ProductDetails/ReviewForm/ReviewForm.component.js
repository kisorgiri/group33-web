import React, { Component } from 'react'
import { Button } from './../../../Common/Button/Button.component'
import { connect } from 'react-redux'
import { addReview_ac } from '../../../../actions/products/product.ac'

const defaultForm = {
    ratingPoint: '',
    ratingMessage: ''
}
class ReviewForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            }
        }
    }
    componentDidMount() {
        this.productId = this.props.match.params['product_id'];

    }

    handleChange = e => {
        let { name, value } = e.target;

        this.setState(preState => ({
            data: {
                ...preState,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.add_review(this.state.data, this.productId);
    }

    render() {
        return (
            <>
                <h2>Add Review</h2>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Point</label>
                    <input type="number" name="ratingPoint" onChange={this.handleChange} className="form-control"></input>
                    <label>Message</label>
                    <input type="text" name="ratingMessage" onChange={this.handleChange} className="form-control"></input>
                    <Button
                        isSubmitting={this.props.isSubmitting}>

                    </Button>
                </form>
            </>
        )
    }
}
const mapStateToProps = rootStore => ({
    isSubmitting: rootStore.product.isSubmitting
})
const mapDispatchToProps = dispatch => ({
    add_review: (data, id) => dispatch(addReview_ac(data, id))
})

export default connect(mapStateToProps, mapDispatchToProps)(ReviewForm)
