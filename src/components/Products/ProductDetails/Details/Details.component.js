import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetch_single_product_ac } from '../../../../actions/products/product.ac'
import { Loader } from '../../../Common/Loader/Loader.component'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css"
const IMG_URL = process.env.REACT_APP_IMG_URL;


const images = [
    {
        original: 'https://picsum.photos/id/1018/1000/600/',
        thumbnail: 'https://picsum.photos/id/1018/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1015/1000/600/',
        thumbnail: 'https://picsum.photos/id/1015/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1019/1000/600/',
        thumbnail: 'https://picsum.photos/id/1019/250/150/',
    },
];

class Details extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }
    componentDidMount() {
        // id will in props
        const productId = this.props.match.params['product_id'];

        this.props.fetchProduct({ _id: productId });
    }

    getImages = (product = []) => {

        const images = [
            {
                original: 'https://picsum.photos/id/1018/1000/600/',
                thumbnail: 'https://picsum.photos/id/1018/250/150/',
            },
            {
                original: 'https://picsum.photos/id/1015/1000/600/',
                thumbnail: 'https://picsum.photos/id/1015/250/150/',
            },
            {
                original: 'https://picsum.photos/id/1019/1000/600/',
                thumbnail: 'https://picsum.photos/id/1019/250/150/',
            },
        ];
        const productImages = (product.images || []).map(img => ({
            original: `${IMG_URL}/${img}`,
            thumbnail: 'https://picsum.photos/id/1018/250/150/',
        }))
        return productImages.length > 0
            ? productImages
            : images;
    }

    render() {
        const { isFetching, product } = this.props
        let content = isFetching
            ? <Loader></Loader>
            : <>
                <h2>Product Details</h2>
                <div className="row">
                    <div className="col-md-6">
                        <ImageGallery items={this.getImages(product)} />;
                    </div>
                    <div className="col-md-6">
                        <h2>{product.name}</h2>
                        <p>Category:{product.category}</p>
                        <p>Tags:{product.tags}</p>
                        <strong>Ratings</strong>
                        {
                            (product.ratings || []).map((rating, index) => (
                                <li>
                                    <h2>{rating.point}</h2>
                                    <p>{rating.message}</p>
                                    <p>{rating.createdAt}</p>
                                    <p>{rating.user.email}</p>
                                </li>
                            ))
                        }
                    </div>
                </div>
            </>
        return content;
    }
}

const mapStateToProps = rootStore => ({
    product: rootStore.product.product,
    isFetching: rootStore.product.isFetching,

})
const mapDispatchToProps = dispatch => ({
    fetchProduct: (params) => dispatch(fetch_single_product_ac(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Details)
