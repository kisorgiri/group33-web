import React, { Component } from 'react'
import DetailsComponent from './Details/Details.component';
import ReviewForm from './ReviewForm/ReviewForm.component';

export class ProductDetailsLanding extends Component {
    constructor() {
        super()


    }

    render() {
        return (
            <>
                <DetailsComponent
                    match={this.props.match}
                >
                </DetailsComponent>
                <ReviewForm match={this.props.match}>

                </ReviewForm>

            </>
        )
    }
}
