// this will be reusable component
// used by add product as well as edit product
import { Button } from './../../Common/Button/Button.component'
import React, { Component } from 'react'
const defaultForm = {
    name: '',
    description: '',
    brand: '',
    category: '',
    modelNo: '',
    price: '',
    color: '',
    status: '',
    size: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    isReturnEligible: '',
    isFeatured: '',
    quantity: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    tags: '',
    manuDate: '',
    expiryDate: ''
}

const IMG_URL = process.env.REACT_APP_IMG_URL;
export class ProductForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            filesToUpload: [],
            selectedFiles: []
        }
    }
    componentDidMount() {
        const { productData } = this.props;
        if (productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...productData,
                    discountedItem: productData.discount && productData.discount.discountedItem ? true : false,
                    discountType: productData.discount && productData.discount.discountType ? productData.discount.discountType : '',
                    discountValue: productData.discount && productData.discount.discountValue ? productData.discount.discountValue : '',
                }
            })
            if (productData.images) {
                let previousImages = productData.images.map(image => (
                    `${IMG_URL}/${image}`
                ))
                this.setState({
                    selectedFiles: previousImages
                })
            }
        }
    }

    handleChange = e => {

        let { name, type, value, checked, files } = e.target;
        if (type === 'file') {
            const { filesToUpload, selectedFiles } = this.state;
            filesToUpload.push(files[0]);
            selectedFiles.push(URL.createObjectURL(files[0]))

            return this.setState({
                filesToUpload,
                selectedFiles
            })
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    removeSelectedImage(index) {
        const { filesToUpload, selectedFiles } = this.state;
        filesToUpload.splice(index, 1)
        selectedFiles.splice(index, 1)
        this.setState({
            filesToUpload,
            selectedFiles
        })
    }

    validateForm = fieldName => {
        let errMsg;

        switch (fieldName) {
            case 'category':
            case 'name':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'requred field*';
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }
    render() {
        const { selectedFiles } = this.state;
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input type="text" value={this.state.data.name} name="name" placeholder="Name" onChange={this.handleChange} className="form-control" />
                    <p className="error">{this.state.error.name}</p>
                    <label>Description</label>
                    <textarea rows="5" value={this.state.data.description} name="description" placeholder="Description here" onChange={this.handleChange} className="form-control"></textarea>
                    <label>Category</label>
                    <input type="text" value={this.state.data.category} name="category" placeholder="Category" onChange={this.handleChange} className="form-control" />
                    <p className="error">{this.state.error.category}</p>

                    <label>Brand</label>
                    <input type="text" value={this.state.data.brand} name="brand" placeholder="Brand" onChange={this.handleChange} className="form-control" />
                    <label>Color</label>
                    <input type="text" value={this.state.data.color} name="color" placeholder="Color" onChange={this.handleChange} className="form-control" />
                    <label>Price</label>
                    <input type="text" value={this.state.data.price} name="price" placeholder="Price" onChange={this.handleChange} className="form-control" />
                    <label>Model No.</label>
                    <input type="text" value={this.state.data.modelNo} name="modelNo" placeholder="Model No." onChange={this.handleChange} className="form-control" />
                    <label>Size</label>
                    <input type="text" value={this.state.data.size} name="size" placeholder="Size" onChange={this.handleChange} className="form-control" />
                    <label>Tags</label>
                    <input type="text" value={this.state.data.tags} name="tags" placeholder="Tags" onChange={this.handleChange} className="form-control" />
                    <input type="checkbox" checked={this.state.data.isFeatured} name="isFeatured" onChange={this.handleChange} />
                    <label> &nbsp;Featured</label>
                    <br />
                    <input type="checkbox" checked={this.state.data.isReturnEligible} name="isReturnEligible" onChange={this.handleChange} />
                    <label> &nbsp;Return Eligible</label>
                    <br />
                    <label>Manu Date</label>
                    <input type="date" value={this.state.data.manuDate} name="manuDate" onChange={this.handleChange} className="form-control" />
                    <label>Expiry Date</label>
                    <input type="date" value={this.state.data.expiryDate} name="expiryDate" onChange={this.handleChange} className="form-control" />
                    <input type="checkbox" checked={this.state.data.discountedItem} name="discountedItem" onChange={this.handleChange} />
                    <label> &nbsp;Discounted Item</label>
                    <br />
                    {
                        this.state.data.discountedItem && (
                            <>
                                <label>Discount Type</label>
                                <select name="discountType" value={this.state.data.discountType} onChange={this.handleChange} className="form-control">
                                    <option disabled value="">(Select Discount Type)</option>
                                    <option value="percentage">Percentage</option>
                                    <option value="quantity">Quantity</option>
                                    <option value="value">Value</option>
                                </select>
                                <label>Discount Value</label>
                                <input type="text" name="discountValue" value={this.state.data.discountValue} placeholder="Discount Value" onChange={this.handleChange} className="form-control" />
                            </>
                        )
                    }

                    <input type="checkbox" name="warrentyStatus" onChange={this.handleChange} />
                    <label> &nbsp;Warrenty Status</label>
                    <br />
                    {
                        this.state.data.warrentyStatus && (
                            <>
                                <label>Warrenty Peroid</label>
                                <input type="text" name="warrentyPeroid" placeholder="Warrenty Peroid" onChange={this.handleChange} className="form-control" />
                            </>
                        )
                    }
                    <label>Choose Image</label>
                    <input type="file" className="form-control" onChange={this.handleChange}></input>
                    <br />
                    {
                        selectedFiles && selectedFiles.length > 0 && (
                            selectedFiles.map((selectedFile, index) => (
                                <React.Fragment key={index}>
                                    <img src={selectedFile} width="200px" alt="product-img.png"></img>
                                    <i
                                        title="remove selection"
                                        style={{ color: 'red', cursor: 'pointer' }}
                                        className="fa fa-trash"
                                        onClick={() => this.removeSelectedImage(index)}
                                    ></i>
                                    <br />
                                </React.Fragment>
                            ))

                        )
                    }

                    <Button
                        isSubmitting={this.props.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    ></Button>

                </form>

            </>
        )
    }
}
