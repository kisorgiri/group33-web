import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDate } from '../../../utils/dateProcessing'
import { Loader } from '../../Common/Loader/Loader.component'
// import action file to seperate http work from component
import { fetchProducts_ac, updateProductsInStore_ac, removeProduct_ac } from './../../../actions/products/product.ac';
// import react-redux to connect with component
import { connect } from 'react-redux';


const IMG_URL = process.env.REACT_APP_IMG_URL;
class ViewProductComponents extends Component {
    constructor() {
        super()
    }

    componentDidMount() {
        console.log('props in view proudct component >>', this.props)
        if (this.props.searchData) {
            this.props.update_products_store(this.props.searchData)
        }
        else {
            this.props.fetch();

        }
    }

    editProduct = (id) => {
        this.props.history.push(`edit_product/${id}`)
    }

    removeProduct = (id, index) => {
        // dilog box || modal
        // ask confirmation
        const confirmation = window.confirm('Are you sure to remove?')
        if (confirmation) {
            this.props.remove(id)

        }
    }

    render() {
        let content = this.props.isLoading
            ? <Loader />
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Tags</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {(this.props.products || []).map((product, index) => (
                        <tr key={product._id}>
                            <td>{index + 1}</td>
                            <td><Link to={`/product_details/${product._id}`}>{product.name}</Link> </td>
                            <td>{product.category}</td>
                            <td>{product.price}</td>
                            <td>{formatDate(product.createdAt, 'YYYY-MM-DD')}</td>
                            <td>{product.tags.length > 0 ? product.tags.join(',') : 'N/A'}</td>
                            <td>
                                <img src={`${IMG_URL}/${product.images[0]}`} width="200px" alt="product_image.png"></img>
                            </td>
                            <td>
                                <i
                                    style={{ color: 'blue', cursor: 'pointer' }}
                                    className="fa fa-pencil"
                                    onClick={() => this.editProduct(product._id)}
                                ></i> &nbsp;
                                <i
                                    style={{ color: 'red', cursor: 'pointer' }}
                                    className="fa fa-trash"
                                    onClick={() => this.removeProduct(product._id, index)}
                                ></i>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        return (
            <>
                <h2>{this.props.searchData ? 'Search Results' : 'View Products'}</h2>
                {
                    this.props.searchData && (
                        <button onClick={this.props.resetSearch} className="btn btn-success">Reset Search</button>
                    )
                }
                {content}
            </>
        )
    }
}

// map state to props
// incoming data inside component from root store
// subscription
// once store data is changed it is refelecte in the component via mapStateToProps
const mapStateToProps = rootStore => ({
    a: 'b',
    c: 'd',
    products: rootStore.product.products,
    isLoading: rootStore.product.isLoading
})

// map dispatch to props
// kun kun action trigger garne vanera define garcha
const mapDispatchToProps = dispatch => ({
    fetch: (params) => dispatch(fetchProducts_ac(params)),
    update_products_store: (data) => dispatch(updateProductsInStore_ac(data)),
    remove: (id) => dispatch(removeProduct_ac(id))
})

export const ViewComponents = connect(mapStateToProps, mapDispatchToProps)(ViewProductComponents);
