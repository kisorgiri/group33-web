import React, { Component } from 'react'
import { handleError } from '../../../utils/handleError'
import { HttpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { ViewComponents } from '../ViewProducts/ViewProducts.component'
import { Button } from './../../Common/Button/Button.component'

const defaultForm = {
    category: '',
    name: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    tags: '',
    multipleDateRange: false
}


export class SearchProduct extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            categories: [],
            allProducts: [],
            names: [],
            searchResults: []
        }
    }

    componentDidMount() {
        HttpClient.POST('/product/search', {})
            .then(response => {
                let cats = [];
                response.data.forEach((item, index) => {
                    if (!cats.includes(item.category)) {
                        cats.push(item.category)
                    }
                })
                this.setState({
                    categories: cats,
                    allProducts: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
    }

    fieldChange = e => {
        let { type, checked, name, value } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        if (name === 'category') {
            this.prepareNames(value);
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate;
        }

        HttpClient.POST(`/product/search`, data)
            .then(response => {
                if (!response.data.length) {
                    notify.showInfo("No any product matched your search query")
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    prepareNames = selectedCategory => {
        const { allProducts } = this.state;
        const names = allProducts.filter(item => item.category === selectedCategory);
        this.setState({
            names
        })
    }

    reset = () => {
        this.setState({
            data: { ...defaultForm },
            searchResults: []
        })
    }

    render() {
        let content = this.state.searchResults.length > 0
            ? <ViewComponents searchData={this.state.searchResults} resetSearch={this.reset}></ViewComponents>
            : <>
                <h2>Search Products</h2>
                <form onSubmit={this.handleSubmit} noValidate className="form-group">
                    <label>Category</label>
                    <select name="category" value={this.state.data.category} onChange={this.fieldChange} className="form-control">
                        <option value="" disabled>(Select Category)</option>
                        {this.state.categories.map((cat, index) => (
                            <option key={index} value={cat}>{cat}</option>
                        ))}
                    </select>
                    {
                        this.state.names.length > 0 && (
                            <>
                                <label>Name</label>
                                <select name="name" value={this.state.data.name} onChange={this.fieldChange} className="form-control">
                                    <option value="" disabled>(Select Name)</option>
                                    {this.state.names.map((item, index) => (
                                        <option key={index} value={item.name}>{item.name}</option>
                                    ))}
                                </select>
                            </>
                        )
                    }
                    <label>Brand</label>
                    <input type="text" name="Brand" placeholder="Brand" onChange={this.fieldChange} className="form-control"></input>
                    <label>Color</label>
                    <input type="text" name="Color" placeholder="Color" onChange={this.fieldChange} className="form-control"></input>
                    <label>Min Price</label>
                    <input type="number" name="minPrice" onChange={this.fieldChange} className="form-control"></input>
                    <label>Max Price</label>
                    <input type="number" name="maxPrice" onChange={this.fieldChange} className="form-control"></input>
                    <label>Select Date</label>
                    <input type="date" name="fromDate" onChange={this.fieldChange} className="form-control"></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.fieldChange}></input>
                    <label> &nbsp; Multiple Date Range</label>
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <br />
                                <label>To Date</label>
                                <input type="date" name="toDate" onChange={this.fieldChange} className="form-control"></input>
                            </>
                        )
                    }

                    <br />
                    <label>Tags</label>
                    <input type="text" name="tags" placeholder="Tags" onChange={this.fieldChange} className="form-control"></input>
                    <Button
                        isSubmitting={this.isSubmitting}
                    ></Button>
                </form>
            </>
        return content
    }
}
