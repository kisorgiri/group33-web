import React from 'react';
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from '../store';
// provider will provde redux store to react application
// react-redux is library to connect redux with react

export const App = () => {

    return (
        <div>
            <ToastContainer />
            <Provider store={store}>
                <AppRouting />
            </Provider>
        </div>

    )
}


// component
// component is basic building block of react
// component can be used as an element which will show disticnt behaviour
// those behaviour can be programmed
// <h2> ==> bold, existing element
// <Kishor> ===> new element created from react

// component will always returns single html node


// component can be written in two ways
// 1.functional component
// 2.class based component

// higher then 16.8  hooks are introduced and class based component are optional

// component can be(in context with data)
// statefull component (class based component)
// ---> class based component 
// (lower then 16.8 version class based Component was solution for statefull Component )
// ====> data should be handle within component
// stateless component (functional component)
// ====> no need to handle data within component


// component's life cycle

// frequenlty used keywords
// props ==> props are incoming data for an component
// state ==> data within component





