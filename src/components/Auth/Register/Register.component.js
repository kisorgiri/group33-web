import React, { Component } from 'react';
import { Button } from './../../Common/Button/Button.component'
import { Link } from 'react-router-dom'
import { notify } from '../../../utils/toastr';
import { HttpClient } from '../../../utils/httpClient';
import { handleError } from '../../../utils/handleError';

const defaultForm = {
    name: '',
    phoneNumber: '',
    email: '',
    username: '',
    password: '',
    confirmPassword: '',
    gender: '',
    dob: '',
    tempAddress: '',
    permanentAddress: ''
}
export class Register extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.runFormValidation(name)
        })
    }

    runFormValidation = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Username must have 6 Characters'
                    : 'required field *'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'required field *'
                break;

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field*'

                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field'
                break;

            default:
                errMsg = '';
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }
    validateFormAgain() {
        const { data } = this.state;

        let hasError = false;
        const errors = {
            username: false,
            password: false,
            email: false
        }
        if (!data.username) {
            errors.username = true;
            hasError = true;
        }
        if (!data.password) {
            errors.password = true;
            hasError = true;

        }
        if (!data.email) {
            errors.email = true;
            hasError = true;

        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                username: errors.username ? 'required field' : null,
                password: errors.password ? 'required field' : null,
                email: errors.email ? 'required field' : null,
            }
        }))

        return hasError;
    }

    handleSubmit = e => {
        e.preventDefault();
        var formHasError = this.validateFormAgain();
        if (formHasError) return;

        this.setState({
            isSubmitting: true
        })
        HttpClient
            .POST('/auth/register', this.state.data)
            .then(response => {
                notify.showInfo("Registration Successfull please check your inbox");
                this.props.history.push("/");
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div className="auth-box">
                <h2> Register</h2>
                <p>Please provide necessary details to register</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" className="form-control" onChange={this.handleChange} />
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.email}</p>
                    <label>Phone Number</label>
                    <input type="number" name="phoneNumber" className="form-control" onChange={this.handleChange} />
                    <label>Username</label>
                    <input type="text" name="username" placeholder="Username" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" name="confirmPassword" placeholder="Confirm Password" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <label>Gender</label>
                    <input type="text" name="gender" placeholder="Gender" className="form-control" onChange={this.handleChange} />
                    <label>Date Of Birth</label>
                    <input type="date" name="dob" className="form-control" onChange={this.handleChange} />
                    <label>Temporary Address</label>
                    <input type="text" name="tempAddress" placeholder="Temporary Address" className="form-control" onChange={this.handleChange} />
                    <label>Permanent Address</label>
                    <input type="text" name="permanentAddress" placeholder="Permanent Address" className="form-control" onChange={this.handleChange} />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    ></Button>
                </form>
                <p>Already Registered?</p>
                <p><Link to="/">back to login</Link></p>
            </div>
        )
    }
}

   // // init
    // componentDidMount() {
    //     // data preparation
    //     // data fetch from API
    //     console.log(' component did mount >>once component is fully loaded')

    // }

    // // update
    // componentDidUpdate(preProps, preState) {
    //     // check for changes in data and perform addition action
    //     console.log('once component is modified')
    //     console.log('previous state >>', preState)
    //     console.log('current state >>', this.state)
    // }

    // componentWillUnmount() {
    //     // all the subscription and async task must be closed in this block

    //     console.log('once component is destroyed')
    //     clearInterval(this.abcd);
    // }
