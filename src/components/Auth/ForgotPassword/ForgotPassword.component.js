import React, { Component } from 'react'
import { handleError } from '../../../utils/handleError'
import { HttpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { Button } from './../../Common/Button/Button.component'
export class ForgotPassword extends Component {
    constructor() {
        super()

        this.state = {
            email: '',
            emailErr: '',
            isSubmitting: false
        }
    }

    change = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        }, () => {
            if (this.state.emailErr) {
                this.validateForm();
            }
        })
    }

    submit = e => {

        e.preventDefault();
        const isValidForm = this.validateForm();
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })
        HttpClient
            .POST('/auth/forgot-password', { email: this.state.email })
            .then(response => {
                notify.showInfo("Password Reset Link sent to your email please check your inbox");
                this.props.history.push('/');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    validateForm = () => {
        let emailErr;
        let validFrom = true;
        if (this.state.email) {
            if (!(this.state.email.includes('@') && this.state.email.includes('.com'))) {
                emailErr = 'Invalid Email';
                validFrom = false;
            }

        } else {
            emailErr = 'Required Field*';
            validFrom = false;

        }
        this.setState({
            emailErr
        })
        return validFrom;
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Forgot Password</h2>
                <p>Please provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.submit}>
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email Address here" className="form-control" onChange={this.change}></input>
                    <p className="error">{this.state.emailErr}</p>
                    <Button
                        isSubmitting={this.state.isSubmitting}
                    ></Button>
                </form>
            </div>
        )
    }
}
