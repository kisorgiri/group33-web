import React, { Component } from 'react'
import { handleError } from '../../../utils/handleError'
import { HttpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { Button } from './../../Common/Button/Button.component'

const defaultForm = {
    password: '',
    confirmPassword: ''
}
export class ResetPassword extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        }
    }
    componentDidMount() {
        this.userId = this.props.match.params['user_id']
    }

    handleChange = e => {
        let { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.runFormValidation(name);
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        HttpClient
            .POST(`/auth/reset-password/${this.userId}`, this.state.data)
            .then(response => {
                notify.showInfo("password reset successfull please login");
                this.props.history.push("/");
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    runFormValidation = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field*'

                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field'
                break;

            default:
                errMsg = '';
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Reset Password</h2>
                <p>Please choose your password</p>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" name="confirmPassword" placeholder="Confirm Password" className="form-control" onChange={this.handleChange} />
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}>

                    </Button>
                </form>
            </div>
        )
    }
}
