// class based component
import { Button } from './../../Common/Button/Button.component'
import React, { Component } from 'react';
import './Login.component.css'
import { Link } from 'react-router-dom'
import { notify } from './../../../utils/toastr'
import { handleError } from '../../../utils/handleError';
import { HttpClient } from '../../../utils/httpClient';

const defaultForm = {
    username: false,
    password: false
}
export class Login extends Component {
    constructor() {
        super(); // parent class ko constructor call
        // initilization
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            remember_me: '',
            isSubmitting: false,
            isValidForm: true
        }
    }

    componentDidMount() {
        const rememberMe = localStorage.getItem('remember_me');
        const hasToken = localStorage.getItem('token')
        if (rememberMe && hasToken) {
            this.props.history.push({
                pathname: '/dashboard/admin'
            })
        }
    }

    handleChange = (e) => {
        let { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            return this.setState({
                remember_me: checked
            })
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            if (this.state.error[name]) {
                this.validateForm();
            }
        })
    }

    validateForm = () => {
        const { data } = this.state;
        let isValid = true;
        let usernameErr = false;
        let passwordErr = false;
        if (!data.username) {
            isValid = false;
            usernameErr = true;
        }
        if (!data.password) {
            isValid = false;
            passwordErr = true;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                username: usernameErr,
                password: passwordErr,
            },
            isValidForm: isValid
        }))
        return isValid;
    }

    handleSubmit(e) {
        e.preventDefault();
        const isValidForm = this.validateForm();
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })
        // API call
        // data extract

        HttpClient
            .POST(`/auth/login`, this.state.data)
            .then(response => {
                console.log('response is >>', response)
                // show toast message
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                // store data in localstorage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user))
                localStorage.setItem('remember_me', this.state.remember_me);
                // navigate to dashboard
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit.bind(this)} noValidate>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" placeholder="Username" name="username" id="username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username && (`required field*`)}</p>
                    <label htmlFor="password"> Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" id="password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password && (`required field`)}</p>

                    <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                    <label>Remember Me</label>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                        enabledLabel="Login"
                        disabledLabel="Loging in..."
                    >

                    </Button>
                </form>
                <p>Don't Have an Account?</p>
                <p className="float-right">Register <Link to="/register">here</Link></p>
                <p className="float-left"> <Link to="/forgot_password">forgot password?</Link></p>
            </div>
        )
    }

}



