import React from 'react';

export const Button = (props) => {
    const disabledLabel = props.disabledLabel || 'submitting...';
    const enabledLabel = props.enabledLabel || 'submit';
    let btn = props.isSubmitting
        ? <button  disabled className="btn btn-info m-t-10" >{disabledLabel}</button>
        : <button disabled={props.isDisabled} className="btn btn-success m-t-10" type="submit">{enabledLabel}</button>
    return btn;
}

// export const ButtonNext = (props) =>  (
//     props.isDisabled
//     ?<button disabled className="btn btn-info m-t-10" >broadway</button>
//     :<button>not disabled</button>
// );
