import React from 'react';
import './SideBar.component.css'
import { Link } from 'react-router-dom';


export const SideBar = (props) => {
    let content = props.isLoggedIn
        ? <div className="sidebar">
            <Link to="/add_product">Add Product</Link>
            <Link to="/view_products">View Product</Link>
            <Link to="/search_product">Search Product</Link>
            <Link to="/message">Messages</Link>
            <Link to="/notification">Notifications</Link>
            <Link to="/profile">Profile</Link>
        </div>
        : null
    return content;
}
