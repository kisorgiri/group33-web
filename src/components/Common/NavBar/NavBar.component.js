import React from 'react';
import './NavBar.component.css';
import { NavLink, withRouter } from 'react-router-dom'
import { redirectToLogin } from '../../../utils/util';
// functional component (stateless)
const logout = (props) => {
    console.log('logout >>', props)
    // localstorage clear
    localStorage.clear();
    redirectToLogin(props);
    // function that will redirect to login
    // navigate to login
}

const NavBarComponent = (props) => {
    // props are incoming data for an component
    // functional component must retrun single html node
    const currentUser = JSON.parse(localStorage.getItem('user'));
    let content = props.isLoggedIn
        ? <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/dashboard">Dashboard</NavLink>
            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/home">Home</NavLink>
            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/about">About</NavLink>

            </li>

            <li className="navItem">
                <button className="btn btn-success logout" onClick={() => logout(props)}>Logout</button>
                <p className="user-info">{currentUser.username}</p>

            </li>
        </ul>
        : <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/home">Home</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/">Login</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/register">Register</NavLink>

            </li>
        </ul>
    return (
        <div className="navBar">
            {content}
        </div>
    )
}

export const NavBar = withRouter(NavBarComponent)
