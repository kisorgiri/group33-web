import React, { Component } from 'react'
import * as io from 'socket.io-client'
import { relativeTime } from '../../../utils/dateProcessing';
import { notify } from '../../../utils/toastr';
import './Messages.component.css'

const socket_url = process.env.REACT_APP_SOCKET_URL;
export class Messages extends Component {
    constructor() {
        super()

        this.state = {
            msgBody: {
                senderId: '',
                receiverId: '',
                senderName: '',
                receiverName: '',
                message: '',
                time: '',
            },
            messages: [],
            users: []
        }
    }
    componentDidMount() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.socket = io(socket_url);
        this.runSocket();
    }
    runSocket = () => {
        this.socket.emit('new-user', this.currentUser.username);
        this.socket.on('reply-message-own', (msg) => {
            const { messages } = this.state;
            messages.push(msg);
            this.setState({
                messages
            })
        })
        // swap sender and receiver when message reached to another client
        this.socket.on('reply-message-for', (msg) => {
            const { messages, msgBody } = this.state;
            msgBody.receiverId = msg.senderId
            msgBody.receiverName = msg.senderName;
            messages.push(msg);
            this.setState({
                messages
            })
        })
        this.socket.on('users', (users) => {
            this.setState({
                users
            })
        })
    }

    handleChange = e => {
        let { name, value } = e.target;
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                [name]: value
            }
        }))
    }

    send = e => {
        e.preventDefault();
        console.log('here at submit')
        const { msgBody, users } = this.state;
        if (!msgBody.receiverId) {
            return notify.showInfo("please select a user to continue")
        }
        msgBody.time = Date.now();
        msgBody.senderName = this.currentUser.username;
        const senderDetails = users.find(user => user.username === this.currentUser.username);
        msgBody.senderId = senderDetails.id;

        this.socket.emit('new-message', msgBody);
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                message: ''
            }
        }))
    }

    selectUser = (user) => {
        console.log('user si s.>>', user)
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                receiverId: user.id,
                receiverName: user.username
            }
        }))
    }


    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <ins>Messages</ins>
                    <div className="chat-box">
                        <ul>
                            {
                                this.state.messages.map((msg, idx) => (
                                    <li style={{ listStyleType: 'none' }} key={idx}>
                                        <h3>{msg.senderName}</h3>
                                        <p>{msg.message}</p>
                                        <small>{relativeTime(msg.time, 'minute')}</small>
                                        <hr />
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    <div>
                        <form onSubmit={this.send} className="form-group">
                            <input style={{ display: 'inline' }} type="text" value={this.state.msgBody.message} name="message" placeholder="Your message here..." onChange={this.handleChange} />
                            <button style={{ display: 'inline' }} type="submit">
                                <i
                                    title="send message"
                                    style={{ color: 'blue', cursor: 'pointer' }}
                                    className="fa fa-paper-plane"
                                ></i> &nbsp;
                            </button>
                        </form>
                    </div>
                </div>
                <div className="col-md-6">
                    <ins>contacts</ins>
                    <div className="chat-box">
                        <ul>
                            {
                                this.state.users.map((user, idx) => (
                                    <li style={{ listStyleType: 'none' }} key={idx}>
                                        <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.username}</button>
                                        <hr />
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
