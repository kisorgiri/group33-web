import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { Messages } from './Common/Messages/Messages.component';
import { NavBar } from './Common/NavBar/NavBar.component';
import { SideBar } from './Common/SideBar/SideBar.component';
import { AddProduct } from './Products/AddProduct/AddProduct.component';
import { EditProduct } from './Products/EditProduct/EditProduct.component';
import { ProductDetailsLanding } from './Products/ProductDetails/ProductDetailsLanding';
import { SearchProduct } from './Products/SearchProducts/SearchProduct.component';
import { ViewComponents } from './Products/ViewProducts/ViewProducts.component';

const Home = (props) => {
    console.log('props in Home is .>', props)
    return (
        <p>Home Page</p>
    )
}

const Dashboard = (props) => {
    console.log('at dashboard >>', props)
    return (
        <>
            <h2>Welcome to Group 33 Store  </h2>
            <p>Please use side navigation menu or contact system administrator for support</p>
        </>
    )
}

const NotFound = (props) => {
    // react fragment
    return (
        <>
            <p>Not Found</p>
            <img src="/images/notfound.png" alt="notfound.png"></img>
        </>
    )
}

// proctected route wrapper
// TODO add design part inside protected and public route
const ProtectedRoute = ({ component: Component, ...rest }) => {
    return localStorage.getItem('token')
        ? <Route {...rest} render={routeProps => (
            <>
                <NavBar isLoggedIn={true}></NavBar>
                <SideBar isLoggedIn={true}></SideBar>
                <div className="main">
                    <Component{...routeProps} ></Component>
                </div>
            </>
        )}></Route>
        : <Redirect to="/"></Redirect>
}

// proctected route wrapper
const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        <>
            <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
            <SideBar isLoggedIn={localStorage.getItem('token') ? true : false}></SideBar>

            <div className="main">
                <Component{...routeProps} ></Component>
            </div>
        </>
    )}></Route>
}



export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact={true} path="/" component={Login}></PublicRoute>
                <PublicRoute exact path="/register" component={Register}></PublicRoute>
                <ProtectedRoute path="/home" component={Home}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewComponents}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct} />
                <ProtectedRoute path="/search_product" component={SearchProduct} />
                <ProtectedRoute path="/message" component={Messages} />
                <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset_password/:user_id" component={ResetPassword}></PublicRoute>
                <PublicRoute path="/product_details/:product_id" component={ProductDetailsLanding}></PublicRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </BrowserRouter>
    )
}

// BrowserRouter is overal routing context supplier
// Route  ==> is configuration block for routing
// every components specified by Route will have three props (supplied by react router dom)
// match
// match is used to take dynamic value from url 
// match>>> params
// history
// history mainy contains functions to navigate goForward,goBack, push
// location
// location is used to take value passed during navigation
// location will have optional url values ?.....
// location.>> search


// react routing
// library ==> react router dom
// BrowserRouter ===> context
// Route ==> configuration
// attributes ==> exact,path, component
// props are supplied in the component 
// history, match and location
// link/NavLink ===> to navigate to page on click action
// to use Link our component must be within scope of BrowserRouter


// protected route
// admin route........


