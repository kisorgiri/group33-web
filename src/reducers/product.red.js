import { PRODUCTS_RECEIVED, SET_IS_SUBMITTING, PRODUCT_RECEIVED, PRODUCT_REMOVED, SET_IS_FETCHING, SET_IS_LOADING } from "../actions/products/type";

const defaultState = {
    products: [],
    isLoading: false,
    isFetching: false,
    isSubmitting: false,
    product: {}
}

export const productReducer = (state = defaultState, action) => {
    console.log('at reducer', action);
    // logic to update state
    switch (action.type) {

        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_IS_SUBMITTING:
            return {
                ...state,
                isSubmitting: action.payload
            }

        case SET_IS_FETCHING:
            return {
                ...state,
                isFetching: action.payload
            }

        case PRODUCT_RECEIVED:
            return {
                ...state,
                product: action.payload
            }

        case PRODUCTS_RECEIVED:
            return {
                ...state,
                products: action.payload
            }
        case PRODUCT_REMOVED:
            const { products } = state;
            products.forEach((product, index) => {
                if (product._id === action.payload) {
                    products.splice(index, 1);
                }
            });
            return {
                ...state,
                products: [...products]
            }

        default:
            return {
                ...state
            }
    }
}
