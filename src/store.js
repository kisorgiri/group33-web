import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk' //middleware
import rootReducer from './reducers';
const middlewares = [thunk];

// intial state TODO
const initialState = {
    product: {
        products: [],
        isLoading: false,
        isFetching: false,
        product: {},
        isSubmitting:false,
    },
    // user: {
    //     users: [],
    //     isLoading: false
    // }
}

export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares))


// var a = {
//     products: [],
//     users: [],
//     notifications: [],
//     isProductLoading: false,
//     isUserLoading: false,
//     isNotificaitonLaoding: false

// }
// var b = {
//     product: {
//         products: [],
//         isLaoding: false,
//         isSubmitting: false
//     },
//     user: {
//         users: [],
//         isLoading: false,
//         isSubmitting: false
//     },
//     notifications: {
//         ...
//     }
// }
