import { notify } from "./toastr";

export const handleError = (error) => {
    debugger;
    let errMsg = 'Something Went Wrong';
    let err = error && error.response;
    let serverResponse = err.data;
    if (serverResponse.msg) {
        errMsg = serverResponse.msg;
    }

    notify.showError(errMsg)
    // application will be paused once debugger is detected
    // steps
    // 1. check error
    // 2. parse error
    // 3. extract error message
    // 4. prepare error message
    // 5. show them in UI using toastr
}
