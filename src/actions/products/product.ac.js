import { handleError } from "../../utils/handleError"
import { notify } from "../../utils/toastr"
import { HttpClient } from "../../utils/httpClient"
import { PRODUCTS_RECEIVED, SET_IS_SUBMITTING, SET_IS_FETCHING, PRODUCT_REMOVED, PRODUCT_RECEIVED, SET_IS_LOADING } from "./type"

// function fetchProducts_ac(params) {
//     // return function (dispatch) { // middleware

//     // }

// }


// export const fetchProducts_ac =(params) =>{
//     return (dispatch)=>{

//     }
// }


export const fetchProducts_ac = params => dispatch => {
    console.log('at action file', params)
    dispatch({
        type: SET_IS_LOADING,
        payload: true
    })
    // http call
    HttpClient.GET('/product', true)
        .then(response => {
            dispatch({
                type: PRODUCTS_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            dispatch({
                type: SET_IS_LOADING,
                payload: false
            })
        })
}

export const updateProductsInStore_ac = (data) => dispatch => {
    console.log('>>>>>>>>>>>>>>>>>>>>>>>.....in action>>>>>>>>>>>>>>>>')
    dispatch({
        type: PRODUCTS_RECEIVED,
        payload: data
    })
}

export const fetch_single_product_ac = (params) => dispatch => {
    dispatch(fetching(true));
    HttpClient.POST('/product/search', { ...params })
        .then(response => {
            dispatch({
                type: PRODUCT_RECEIVED,
                payload: response.data[0]
            })
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            dispatch(fetching(false))
        })
}

export const removeProduct_ac = (id) => dispatch => {

    HttpClient
        .DELETE(`/product/${id}`, true)
        .then(response => {
            notify.showSuccess("Product Removed Successfully")
            dispatch({
                type: PRODUCT_REMOVED,
                payload: id
            })
        })
        .catch(err => {
            handleError(err);
        })
}

export const addReview_ac = (data, product_id) => dispatch => {

    HttpClient
        .POST(`/product/add-rating/${product_id}`, data, true)
        .then(response => {
            notify.showSuccess("Ratings Added")
            dispatch({
                type: PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            handleError(err);
        })
}


const fetching = isFetching => ({
    type: SET_IS_FETCHING,
    payload: isFetching
})

const submitting = isSubmitting => ({
    type: SET_IS_SUBMITTING,
    payload: isSubmitting
})





